﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace StoreAutomation.Helper_Class
{
    public static class ProductHelper
    {
        public static IWebElement GetProductLink(IList<IWebElement> CartProducts,string productName)
        {
            for (int i = 0; i < CartProducts.Count; i++)
            {
                if (CartProducts[i].FindElement(By.LinkText(productName)).Displayed)
                {
                    return CartProducts[i].FindElement(By.LinkText(productName));
                }
            }

            return null;
        }
    }
}
