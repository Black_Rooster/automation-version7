﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using StoreAutomation.Component;
using System;

namespace StoreAutomation.Page_Model
{
    public class Base
    {
        public IWebDriver Driver;
        public Actions Actions;
        public WebDriverWait DriverWait;

        public Base(IWebDriver driver)
        {
            Driver = driver;
            DriverWait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
            Actions = new Actions(Driver);
        }

        public IWebElement SearchField => Driver.FindElement(By.Name("search_query"));

        public IWebElement SearchButton => Driver.FindElement(By.Name("submit_search"));

        public IWebElement ViewCart => Driver.FindElement(By.XPath("//a[@title='View my shopping cart']"));

        public string CurrentURL => Driver.Url;

        public AddToCartModal AddToCartModal => new AddToCartModal(Driver);

        public IWebElement AddToCart(int dataIdProduct)
        {
            return Driver.FindElement(By.XPath($"//a[@data-id-product='{dataIdProduct}']"));
        }

        public void HoverEffect(IWebElement element)
        {
            Actions.MoveToElement(element).Build().Perform();
        }
    }
}
