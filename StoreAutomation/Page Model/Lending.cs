﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace StoreAutomation.Page_Model
{
    public class Lending : Base
    {
        public Lending(IWebDriver driver) : base(driver)
        {

        }

        public IList<IWebElement> ProductList => Driver.FindElements(By.XPath("//ul[@id='homefeatured']//li"));

        public IWebElement SeleniumFrameworkLink => Driver.FindElement(By.LinkText("Selenium Framework"));
    }
}
