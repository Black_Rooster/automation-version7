﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace StoreAutomation.Page_Model
{
    public class Cart : Base
    {
        public Cart(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement ProductCounter => Driver.FindElement(By.ClassName("ajax_cart_no_product"));

        public IWebElement RemoveProduct => Driver.FindElement(By.XPath("//a[@title='Delete']"));

        public IList<IWebElement> CartProducts => Driver.FindElements(By.XPath("//table[@id='cart_summary']//tbody//tr"));

        public IWebElement ProductTotal => Driver.FindElement(By.Id("total_product"));

        public IList<IWebElement> ProductPriceList => Driver.FindElements(By.XPath("//td[@data-title='Total']//span"));

        public void WaitForProductCounterUpdate()
        {
            DriverWait.Until(value => ProductCounter.Displayed == true);
        }

        public void WaifForProducts()
        {
            DriverWait.Until(results => ProductPriceList.Count == 2);
        }
    }
}
