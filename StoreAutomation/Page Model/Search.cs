﻿using OpenQA.Selenium;

namespace StoreAutomation.Page_Model
{
    public class Search : Base
    {
        public Search(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement GetProductLink(string productName)
        {
            return Driver.FindElement(By.XPath($"//a[@title='{productName}' and @class='product-name']"));
        }
    }
}
