﻿using NUnit.Framework;

namespace StoreAutomation.Test
{
    [TestFixture]
    public class SendToFriend : Base
    {
        [SetUp]
        public void OpenSendToFriendModal()
        {
            var productName = "Faded Short Sleeve T-shirts";

            StoreSetup.BaseComponentPage.SearchField.SendKeys(productName);
            StoreSetup.BaseComponentPage.SearchButton.Click();
            StoreSetup.SearchPage.GetProductLink(productName).Click();
            StoreSetup.ProductDetailPage.SendToFriendLink.Click();
        }

        [Test]
        public void GivenValidInput_WhenSendingProductToAFriend_ThenDisplaySuccessModal()
        {
            StoreSetup.ProductDetailPage.FriendNameInput.SendKeys("senzo");
            StoreSetup.ProductDetailPage.FriendEmailInput.SendKeys("senzo@gmail.com");
            StoreSetup.ProductDetailPage.SendEmailButton.Click();

            Assert.IsTrue(StoreSetup.ProductDetailPage.SuccessModal.Displayed, "Product not sent to a friend");
        }

        [Test]
        public void GivenEmptyField_WhenSendingProductToAFriend_ThenShowErrorMessage()
        {
            StoreSetup.ProductDetailPage.SendEmailButton.Click();

            Assert.AreEqual("You did not fill required fields", StoreSetup.ProductDetailPage.ErrorMessage.Text);
        }
    }
}
