﻿using NUnit.Framework;
using StoreAutomation.Helper_Class;

namespace StoreAutomation.Test
{
    [TestFixture]
    public class ProductTotal : Base
    {
        [Test]
        public void GivenTwoProductInACart_WhenConfirmingTotal_ThenAddTheirPriceAndCompareItWithTotalPrice()
        {
            StoreSetup.BaseComponentPage.HoverEffect(StoreSetup.LendingPage.ProductList[0]);
            StoreSetup.BaseComponentPage.AddToCart(1).Click();
            StoreSetup.BaseComponentPage.AddToCartModal.Close.Click();

            StoreSetup.BaseComponentPage.HoverEffect(StoreSetup.LendingPage.ProductList[1]);
            StoreSetup.BaseComponentPage.AddToCart(2).Click();
            StoreSetup.BaseComponentPage.AddToCartModal.Close.Click();

            StoreSetup.BaseComponentPage.ViewCart.Click();

            var productTotal = TotalHelper.GetProductTotalPrice(StoreSetup.CartPage.ProductPriceList);
            
            Assert.AreEqual(StoreSetup.CartPage.ProductTotal.Text, productTotal);
        }
    }
}
