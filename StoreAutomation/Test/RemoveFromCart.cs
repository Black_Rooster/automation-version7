﻿using NUnit.Framework;

namespace StoreAutomation.Test
{
    [TestFixture]
    public class RemoveFromCart : Base
    {
        [Test]
        public void GivenProductName_WhenRemovingProductInTheCart_ThenRemoveProduct()
        {
            var productName = "Faded Short Sleeve T-shirts";
            
            StoreSetup.BaseComponentPage.SearchField.SendKeys(productName);
            StoreSetup.BaseComponentPage.SearchButton.Click();
            StoreSetup.SearchPage.GetProductLink(productName).Click();
            StoreSetup.ProductDetailPage.AddToCartButton.Click();
            StoreSetup.SearchPage.AddToCartModal.ProceedToCheckoutButton.Click();
            StoreSetup.CartPage.RemoveProduct.Click();
            StoreSetup.CartPage.WaitForProductCounterUpdate();

            Assert.AreEqual("(empty)", StoreSetup.CartPage.ProductCounter.GetAttribute("textContent"));
        }
    }
}
