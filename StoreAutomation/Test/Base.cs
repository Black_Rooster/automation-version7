﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace StoreAutomation.Test
{
    public class Base
    {
        private IWebDriver _driver;
        private readonly int _waitingTime = 30;

        [SetUp]
        public void DriverSetup()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(_waitingTime);

            StoreSetup.PageInitializer(_driver);
        }

        [TearDown]
        public void DriverShutdown()
        {
            _driver.Dispose();
        }
    }
}
