﻿using NUnit.Framework;
using StoreAutomation.Helper_Class;

namespace StoreAutomation.Test
{
    [TestFixture]
    public class AddToCart : Base
    {
        private readonly string _productName = "Faded Short Sleeve T-shirts";
        private readonly string _errorMessage = "Product was not added";

        [SetUp]
        public void SearchProduct()
        {
            StoreSetup.BaseComponentPage.SearchField.SendKeys(_productName);
            StoreSetup.BaseComponentPage.SearchButton.Click();
        }

        [Test]
        public void GivenProductName_WhenAdddingToCartWithinProductDetails_ThenAddProductToCart()
        {
            StoreSetup.SearchPage.GetProductLink(_productName).Click();
            StoreSetup.ProductDetailPage.AddToCartButton.Click();
            StoreSetup.BaseComponentPage.AddToCartModal.ProceedToCheckoutButton.Click();

            Assert.IsTrue(ProductHelper.GetProductLink(StoreSetup.CartPage.CartProducts, _productName).Displayed, _errorMessage);
        }

        [Test]
        public void GivenProductName_WhenAdddingToCartFromSearchResult_ThenAddProductToCart()
        {
            StoreSetup.BaseComponentPage.HoverEffect(StoreSetup.SearchPage.GetProductLink(_productName));
            StoreSetup.BaseComponentPage.AddToCart(1).Click();
            StoreSetup.ProductDetailPage.AddToCartModal.ProceedToCheckoutButton.Click();

            Assert.IsTrue(ProductHelper.GetProductLink(StoreSetup.CartPage.CartProducts, _productName).Displayed, _errorMessage);
        }
    }
}
