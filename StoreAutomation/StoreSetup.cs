﻿using OpenQA.Selenium;
using StoreAutomation.Page_Model;

namespace StoreAutomation
{
    public static class StoreSetup
    {
        public static Cart CartPage;
        public static ProductDetail ProductDetailPage;
        public static Search SearchPage;
        public static Base BaseComponentPage;
        public static Lending LendingPage;

        public static void PageInitializer(IWebDriver driver)
        {
            driver.Navigate().GoToUrl("http://automationpractice.com/index.php");

            BaseComponentPage = new Base(driver);
            CartPage = new Cart(driver);
            ProductDetailPage = new ProductDetail(driver);
            SearchPage = new Search(driver);
            LendingPage = new Lending(driver);
        }
    }
}
