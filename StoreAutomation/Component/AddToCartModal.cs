﻿using OpenQA.Selenium;

namespace StoreAutomation.Component
{
    public class AddToCartModal
    {
        private readonly IWebDriver _driver;

        public AddToCartModal(IWebDriver driver)
        {
            _driver = driver;
        }

        public IWebElement ProceedToCheckoutButton => _driver.FindElement(By.XPath("//a[@title='Proceed to checkout']"));

        public IWebElement Close => _driver.FindElement(By.ClassName("cross"));

    }
}
